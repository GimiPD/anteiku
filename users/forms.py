from django.contrib.auth.forms import UserCreationForm
from django.forms import TextInput
from users.models import ExtendUser


class UserCreateForm(UserCreationForm):
    class Meta:
        model = ExtendUser
        fields = ['first_name', 'last_name', 'username', 'email', 'phone_number', 'address',
                  'city', 'country', 'postal_code']
        widgets = {
            'first_name': TextInput(attrs={
                'placeholder': 'Please type your first name here',
                'class': 'input is-primary'
            }),
            'last_name': TextInput(attrs={
                'placeholder': 'Please type your last name here',
                'class': 'input is-primary'
            }),
            'username': TextInput(attrs={
                'placeholder': 'Please type your username here',
                'class': 'input is-primary'
            }),
            'email': TextInput(attrs={
                'placeholder': 'Please type your email here',
                'class': 'input is-primary'
            }),
            'phone_number': TextInput(attrs={
                'placeholder': 'Please type your phone number here',
                'class': 'input is-primary'
            }),
            'address': TextInput(attrs={
                'placeholder': 'Please type your address here',
                'class': 'input is-primary'
            }),
            'city': TextInput(attrs={
                'placeholder': 'Please type your city here',
                'class': 'input is-primary'
            }),
            'country': TextInput(attrs={
                'placeholder': 'Please type your country here',
                'class': 'input is-primary'
            }),
            'postal_code': TextInput(attrs={
                'placeholder': 'Please type your postal code here',
                'class': 'input is-primary'
            }),
        }

    def __init__(self, *args, **kwargs):
        super(UserCreateForm, self).__init__(*args, **kwargs)
        self.fields['password1'].widget.attrs['class'] = 'input is-primary'
        self.fields['password1'].widget.attrs['placeholder'] = 'Please insert password'
        self.fields['password2'].widget.attrs['class'] = 'input is-primary'
        self.fields['password2'].widget.attrs['placeholder'] = 'Please insert password confirmation'

    def clean(self):
        cleaned_data = self.cleaned_data
        username = cleaned_data.get('username')
        email = cleaned_data.get('email')
        all_users = ExtendUser.objects.all()
        for user in all_users:
            if username == user.username:
                msg = 'username already taken'
                self._errors['username'] = self.error_class([msg])
            if email == user.email:
                msg = 'this email is already connected to an account'
                self._errors['email'] = self.error_class([msg])
        return cleaned_data
