from creditcards.models import CardNumberField, CardExpiryField, SecurityCodeField
from django.contrib.auth.models import User
from django.db import models


class ExtendUser(User):
    phone_number = models.CharField(max_length=100)
    address = models.CharField(max_length=450)
    city = models.CharField(max_length=100)
    country = models.CharField(max_length=100)
    postal_code = models.CharField(max_length=100)


class Payment(models.Model):
    cc_type = models.CharField(max_length=100)
    cc_number = CardNumberField('card number')
    cc_expiry = CardExpiryField('expiration date')
    cc_code = SecurityCodeField('security code')

    def __str__(self):
        return self.cc_type
