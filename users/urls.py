from django.urls import path
from users import views


urlpatterns = [
    path('user-sign-up/', views.sign_up, name='users-create')
]