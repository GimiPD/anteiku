from django.urls import path
from home.views import HomePageTemplateView


urlpatterns = [
    path('', HomePageTemplateView.as_view(), name='home')
]
