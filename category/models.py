from django.db import models


class Category(models.Model):
    category_name = models.CharField(max_length=150)
    category_description = models.CharField(max_length=450)
    category_image = models.ImageField(blank=True, null=True)

    def __str__(self):
        return self.category_name


class SubCategory(models.Model):
    sub_category_name = models.CharField(max_length=150)
    sub_category_description = models.CharField(max_length=450)

    def __str__(self):
        return self.sub_category_name
