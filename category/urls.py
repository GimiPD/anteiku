from django.urls import path
from category.views import CategoryCreateView, CategoryListView, CategoryUpdateView, \
    CategoryDeleteView, CategoryDetailView


urlpatterns = [
    path('category-add/', CategoryCreateView.as_view(), name='category_add'),
    path('category-list/', CategoryListView.as_view(), name='category_list'),
    path('category-edit/<int:pk>/', CategoryUpdateView.as_view(), name='category_edit'),
    path('category-delete/<int:pk>/', CategoryDeleteView.as_view(), name='category_delete'),
    path('category-details/<int:pk>/', CategoryDetailView.as_view(), name='category_details'),
]
