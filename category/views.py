from django.urls import reverse_lazy
from django.views.generic import CreateView, ListView, UpdateView, DeleteView, DetailView
from category.forms import CategoryCreateForm
from category.models import Category


class CategoryCreateView(CreateView):
    template_name = 'category/category_add.html'
    model = Category
    form_class = CategoryCreateForm
    success_url = reverse_lazy('category_list')


class CategoryListView(ListView):
    template_name = 'category/category_list.html'
    model = Category
    context_object_name = 'all_categories'


class CategoryUpdateView(UpdateView):
    template_name = 'category/category_edit.html'
    model = Category
    fields = '__all__'
    success_url = reverse_lazy('category_list')


class CategoryDeleteView(DeleteView):
    template_name = 'category/category_delete.html'
    model = Category
    success_url = reverse_lazy('category_list')


class CategoryDetailView(DetailView):
    template_name = 'category/category_details.html'
    model = Category
    context_object_name = 'category_details'
